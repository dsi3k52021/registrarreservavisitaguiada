#from tkinter import *
import tkinter as tk
from tkinter import ttk

def seleccionTipoVisita():
    labeltoptipovisita = tk.Label(mi_Frame,
                                  text = "Seleccione Tipo de Visita:")
    labeltoptipovisita.grid(column=0, row=24,padx=1,pady=50)

    variabletipovisita = tk.StringVar()

    radiobuttontipovisita = tk.Radiobutton(mi_Frame,text="Completa", variable=variabletipovisita, value=1)
    radiobuttontipovisita1 = tk.Radiobutton(mi_Frame,text="Exposición", variable=variabletipovisita, value=2)
    print(dict(radiobuttontipovisita))
    radiobuttontipovisita.grid(column=1, row=24,padx=1,pady=5)
    radiobuttontipovisita1.grid(column=2, row=24,padx=1,pady=5)

    print(variabletipovisita)


def mostrarComboSede():
    labelTopSede = tk.Label(mi_Frame,
                            text = "Seleccione la Sede:")
    labelTopSede.grid(column=0, row=23,padx=1,pady=50)


    comboSede = ttk.Combobox(mi_Frame,
                             values=[
                                 "Sede1",
                                 "Sede2",
                                 "Sede3",
                                 "Sede4"])
    print(dict(comboSede))
    comboSede.grid(column=1, row=23,padx=1,pady=5)
    comboSede.current(0)

    print(comboSede.current(), comboSede.get())


def ingresarVisitantes():


    labelTop1 = tk.Label(mi_Frame,
                         text = "Ingrese la cantidad de visitantes:")
    labelTop1.grid(column=0, row=22,padx=10,pady=5)
    w = ttk.Spinbox(mi_Frame, from_=0, to=2000,width=10)
    w.grid(column=1, row=22,padx=10,pady=5)





def mostrarComboEscuela():
    labelTop = tk.Label(mi_Frame,
                        text = "Seleccione la escuela:")
    labelTop.grid(column=0, row=21,padx=1,pady=50)


    comboExample = ttk.Combobox(mi_Frame,
                                values=[
                                    "Carbo",
                                    "Inmaculada Concepcion",
                                    "Cassafus",
                                    "Aleman"])
    print(dict(comboExample))
    comboExample.grid(column=1, row=21,padx=1,pady=5)
    comboExample.current(0)

    print(comboExample.current(), comboExample.get())

def registrar():
    mi_Frame.pack(fill="x")
    mostrarComboEscuela()
    ingresarVisitantes()
    mostrarComboSede()
    seleccionTipoVisita()


def cancelarFrame():
    mi_Frame.pack_forget()

def salir():
    ventana.destroy()

ventana = tk.Tk()
ventana.geometry("600x600")
ventana.title("Registrar reserva de visitas guiadas")
mi_Frame = tk.Frame() #Creación del Frame
mi_Frame.pack(fill="both") #Empaquetamiento del Frame
#mi_Frame.place(x=10,y=50)
mi_Frame.config(width="500", height="500")
#mi_Frame.config(bg="blue")
mi_Frame.config(bd=24)
mi_Frame.config(relief="sunken")
#mi_Frame.config(cursor="heart")
boton1 = tk.Button(ventana,text="Registrar reserva de visitas guiadas",command=registrar)
boton1.place(x=25,y=10)
boton2=tk.Button(ventana,text="Cancelar",command=cancelarFrame)
boton2.place(x=350,y=10)
boton3=tk.Button(ventana,text="Salir",command=salir)
boton3.place(x=450,y=10)
ventana.mainloop()

